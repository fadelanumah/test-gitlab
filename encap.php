<?php

class person {
    public $nama;
    private $umur;

    public function __construct($nama, $umur) {
        $this->nama = $nama;
        $this->umur = $umur;
    }

    // private hanya dapat diakses dari dalam kelas tersebut
    public function setUmur($umur) {
        $this->umur = $umur;
    }
}

$obj = new person('me', 20);
$obj->nama = 'fadel';

// akan error jika langsung di rubah seperti di bawah
// $obj->umur = 21;
$obj->setUmur(21);
var_dump($obj);