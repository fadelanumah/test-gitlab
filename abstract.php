<?php

// kita dapat membuat abstraction dengan menggunakan keyword abstract

abstract class person{
	public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

	// mendeklarasikan abstract function
	abstract protected function greeting();
}

// saat diturunkan
class intern extends person {
	
	// kita bisa lihat pada parentClass function functionName memiliki access modifier protected.
	// kita harus menurunkannya dengan access modifier yang sama atau di bawahnya
	// protected atau public
	public function greeting() {
        echo "halo, saya {$this->name}. saya Intern";
    }
}

$intern = new intern('Fadel');
$intern->greeting();