<?php

// echo "Hello PHP";

class food {
    public $name;
    public $taste;
    public $price;

    // contructor
    public function __construct($name, $taste, $price)
    {
        $this->name = $name;
        $this->taste= $taste;
        $this->price = $price;
    }

    // destruct akan otomatis dijalankan di akhir
    public function __destruct() {
        echo "ini destruct";
    }

    public function helloFood() {
        echo "Nama makanan  :  {$this->name}".PHP_EOL;
        echo "Rasa makanan  :  {$this->taste}".PHP_EOL;
        echo "Harga makanan :  {$this->price}".PHP_EOL;
    }
}

// mewariskan (inherited) dari class food
class burgir extends food {
    public function getName() {
        echo "Nama Makanan : {$this->name}".PHP_EOL;
    }
}

$burgir = new burgir('Burgir', 'Good', 12000);
$burgir->helloFood();
// print menggunakan fungsi yang dimiliki oleh food
$burgir->getName();