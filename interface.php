<?php

// interface mirip seperti abstract

Interface buah {
    public function greeting();
}

class mangga implements buah {
    public function greeting() {
        echo "Halo, saya mangga".PHP_EOL;
    }
}

class jeruk implements buah {
    public function greeting()
    {
        echo "Halo, saya jeruk".PHP_EOL;
    }
}

class nanas implements buah {
    public function greeting()
    {
        echo "Halo, saya nanas".PHP_EOL;
    }
}

$mangga = new mangga();
$apel = new apel();
$nanas = new nanas();
$buahs = array($mangga, $apel, $nanas);

foreach ($buahs as $buah) {
    $buah->greeting();
}

// seperti namanya polymorphisme artinya adalah banyak bentuk
// kita bisa merubah - ubah output dari fungsi yang sama di kelas lain
// kita juga bisa merubah parameternya dan bentuk lainnya walau fungsinya sama.